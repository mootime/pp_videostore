﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioFX
{
    public static void FXOneShot(string audioclip, GameObject audiosourceobj)
    {

        //plays one sound effect from the sfx folder (no need to enter /sfx to every input :))
        if(audiosourceobj != null && audiosourceobj.GetComponent<AudioSource>() != null){
            audiosourceobj.GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("SFX/" + audioclip));
        }
    }//FXOneShot

    public static IEnumerator AudioFade(AudioSource audiosource, float duration, float targetvol){

        float currentTime = 0;
        float start = audiosource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audiosource.volume = Mathf.Lerp(start, targetvol, currentTime / duration);
            yield return null;
        }
        yield break;
    }//AudioFade

}//eof
