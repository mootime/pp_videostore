﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventSystem : MonoBehaviour
{

    //!!!PLEASE PLEASE PLEASE remember to add using system to ALL scripts that call an event!!!

    public static EventSystem Event;

    //make sure if you use and event system again to have it start in an awake fuction so it starts before start functions!
    void Awake()
    {

      Event = this;

    }

    public event Action onDisablePlayer;
    public event Action onEnablePlayer;
    public event Action<Vector3, float> onMovePlayerSmooth;
    public event Action<Vector3, float> onMoveViewSmooth;
    public event Action<GameObject> onTaskCompleted;
    public event Action<string, int> onSubTaskCompleted;
    public event Action<string> onKeyFound;
    public event Action<float, Vector3> ondidhear_noise;
    public event Action<string> onpush_toast;
    public event Action onAllTaskComplete;


    public void DisablePlayer(){

      onDisablePlayer();  

    }

    public void EnablePlayer(){

      onEnablePlayer();  

    }

    public void MovePlayerSmooth(Vector3 location, float time){

      onMovePlayerSmooth(location, time);

    }

    public void MoveViewSmooth(Vector3 location, float time){

      onMoveViewSmooth(location, time);

    }

    public void TaskCompleted(GameObject task){

      onTaskCompleted(task);

    }

    public void SubTaskCompleted(string parenttask, int stepnumber){
      if(onSubTaskCompleted != null){
        onSubTaskCompleted(parenttask, stepnumber);
      }

    }

    public void KeyFound(string key){

      onKeyFound(key);


    }

    public void didhear_noise (float loudness, Vector3 location){

      ondidhear_noise(loudness, location);

    }

    public void push_toast(string message){

      onpush_toast(message);

    }

    public void AllTaskComplete(){

      onAllTaskComplete();  

    }

}
