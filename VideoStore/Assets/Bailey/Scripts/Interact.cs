﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{

    public float interact_dist = 1.5f;
    RaycastHit interacthit;
    public string interact_text = "";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //RaycastHit interacthit;
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.yellow);

       if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out interacthit, interact_dist, ~(1 << 10) )){

           //display ui interact.hit -> get door text
           interact_text = get_ui_message();
           //EventSystem.Event.push_toast(interact_text);

           if(Input.GetKey(KeyCode.E)){

                if(interacthit.transform.tag == "DoorTrigger"){

                    //interacthit.transform.GetComponent<Door>().door_ctrl();

                }

                if(interacthit.transform.tag == "GenericInteract"){

                    interacthit.transform.GetComponent<Interactables>().Interacted();

                }

                if(interacthit.transform.tag == "OpenContainer"){

                    //interacthit.transform.GetComponent<OpenContainer>().Interacted();

                }

           }//press e

       }
       else{

           //EventSystem.Event.push_toast("");

       }

    }//fixed

    string get_ui_message(){

        switch(interacthit.transform.tag){

        case "GenericInteract":
            return  interacthit.transform.GetComponent<Interactables>().iname;
        break;

        /*case "DoorTrigger":
            return interacthit.transform.GetComponent<Door>().doortext;
        break;

        case "OpenContainer":
            return interacthit.transform.GetComponent<OpenContainer>().conttext;
        break;*/

        default:
        return "";
        break;

        }
        
    }

}//eof
