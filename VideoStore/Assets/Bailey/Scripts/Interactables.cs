﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Interactables : MonoBehaviour
{

    public string iname;
    public bool iskey = false;
    public string parenttask;
    public int stepnumber;

    public bool racted = false;

    //genaric pickup script for keys and tasks/subtasks

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Interacted() {

        if(racted == false){
            if(iskey == true){

                Debug.Log("Picked up key " + iname);
                EventSystem.Event.KeyFound(iname);

            }
            else{

                EventSystem.Event.SubTaskCompleted(parenttask, stepnumber);

            }
        }

        racted = true;
        Destroy(this.gameObject);

    }
}
