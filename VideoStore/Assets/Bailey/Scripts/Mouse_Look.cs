﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse_Look : MonoBehaviour
{  
    public float mousespeed = 2.5f;
    private float mouse_y = 0;

    // Start is called before the first frame update
    void Start()
    {

    //Turn off mouse
    //Cursor.visible = false;
    //Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {

        transform.parent.Rotate(0, Input.GetAxis("Mouse X") * mousespeed ,0);
        mouse_y -= Input.GetAxis("Mouse Y") * mousespeed;
        mouse_y = Mathf.Clamp(mouse_y, -38, 38);
        transform.localEulerAngles = new Vector3(mouse_y, transform.localEulerAngles.y, 0);

    }
}
