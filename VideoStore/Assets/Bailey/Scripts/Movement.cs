﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System;

public class Movement : MonoBehaviour
{

    public bool can_move = true;
    public float speed = 4.0f;
    CharacterController controller;
    public GameObject PCam;
    public Vector3 movevector;
    public bool crouched = false;

    public float stamina = 200.0f;
    public bool stamina_cooldown = false;

    //concrete, glass, water, steel || must match resource folder!
    public string floortype = "Concrete";
    //slower for crouch, faster for run
    public float stepspeed = 0.7f;
    public Object[] steplib;
    public bool stepshot = false;

    public float f_multi = 0f;
    public float detection_level = 100.0f;

    // Start is called before the first frame update
    void Start()
    {

        controller = GetComponent<CharacterController>();
        PCam = GameObject.Find(this.transform.name + "/PlayerCamera");

        EventSystem.Event.onDisablePlayer += DisablePlayer;
        EventSystem.Event.onEnablePlayer += EnablePlayer;
        EventSystem.Event.onMovePlayerSmooth += MovePlayerSmooth;
        EventSystem.Event.onMoveViewSmooth += MovePlayerSmooth;

        //just in case step surface is null/ unset
        steplib = Resources.LoadAll("SFX/Footsteeps/" + floortype + "/", typeof(AudioClip));


    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(can_move == true){
            movevector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            controller.SimpleMove(transform.TransformDirection(Vector3.ClampMagnitude(movevector, 1)) * speed);

            //sprint
            if(Input.GetKey(KeyCode.LeftShift) && stamina > 0 && stamina_cooldown != true && crouched == false){

                stamina = stamina - 0.86f;
                speed = 8f; 
                stepspeed = 0.4f;

            }
            else{

                if(crouched == false){
                    speed = 4f;
                    stepspeed = 0.7f;
                }

                if(stamina < 200f){

                stamina = stamina + 0.4f;  

                }
            }

            if(stamina <= 0){

                stamina_cooldown = true;
                stamina = 0;
                StartCoroutine(WaitCo(5.2f));

            }

            //crouch

            RaycastHit hitp;
            RaycastHit hitn;

            if(Input.GetKeyDown(KeyCode.C) && crouched == false && speed == 4){

                crouched = true;
                controller.height = 0.5f;
                PCam.transform.localPosition = new Vector3(0f,0.2f,-0.162f);
                speed = speed/2;
                stepspeed = 1.2f;

            }

            //this part checks for a situation in which the user lets go of the crouch key but is still ducked underneath, two raycasts are used in oppsoite corners to reduce buggyness when comeing out from under something
            if(Physics.Raycast (new Vector3(this.transform.position.x + 0.2f, this.transform.position.y, this.transform.position.z + 0.2f), Vector3.up, out hitp) && Physics.Raycast (new Vector3(this.transform.position.x - 0.2f, this.transform.position.y, this.transform.position.z - 0.2f), Vector3.up, out hitn)){
            //Debug.Log(hitp.distance);
            //Debug.DrawRay(Vector3(this.transform.position.x + 0.2, this.transform.position.y, this.transform.position.z + 0.2), Vector3.up, Color.blue);
            //Debug.DrawRay(Vector3(this.transform.position.x - 0.2, this.transform.position.y, this.transform.position.z - 0.2), Vector3.up, Color.blue);
            if(Input.GetKeyUp(KeyCode.C)){
                if(hitp.distance >= 0.56f && hitn.distance >= 0.56f){
                    crouched = false;
                    controller.height = 1.8f;
                    PCam.transform.localPosition = new Vector3(0f,0.474f,-0.162f);
                    speed = 4f;
                    stepspeed = 0.7f;
                    return;
                }
            }

                if(crouched == true && !Input.GetKey(KeyCode.C) && hitp.distance > 0.56f && hitn.distance >= 0.56f){
                    crouched = false;
                    controller.height = 1.8f;
                    PCam.transform.localPosition = new Vector3(0f,0.474f,-0.162f);
                    speed = 4f;
                    stepspeed = 0.7f;
                }
            }//crouch cast

            //footsteps
            if(movevector != Vector3.zero && stepshot == false){

                stepshot = true;
                StartCoroutine(FootStepWait());

            }

            RaycastHit hitf;
            //1 << 9

            Debug.DrawRay(transform.position, Vector3.down, Color.red);
            //only casts agaist layer 9 (floor detect)
            if(Physics.Raycast (transform.position, Vector3.down, out hitf, 1.5f, 1 << 9)){

                floortype = hitf.transform.tag;
                steplib = Resources.LoadAll("SFX/Footsteeps/" + floortype + "/", typeof(AudioClip));

                switch(floortype){

                    case "Concrete":
                    f_multi = 11.5f;
                    break;

                    case "Steel":
                    f_multi = 8.5f;
                    break;

                }

                Debug.Log(hitf.transform.tag);

            }

            //sound detection
            if(movevector != Vector3.zero){
                detection_level = f_multi * stepspeed;
            }
            else{

                detection_level = 100f;

            }

            //next movement thing

        }//canmove  
    }//fixedupdate

    public void DisablePlayer(){

        controller.enabled = false;
        can_move = false;

    }

    
    public void EnablePlayer(){

        controller.enabled = true;
        can_move = true;

    }

    public void MovePlayerSmooth(Vector3 location, float time){

       LeanTween.move(this.gameObject, location, time).setEase(LeanTweenType.easeInQuart); 

    }

    public void MoveViewSmooth(Vector3 location, float time){

       LeanTween.move(this.gameObject, location, time).setEase(LeanTweenType.easeInQuart); 

    }

    IEnumerator WaitCo(float t){

        yield return new WaitForSeconds(t);
        stamina_cooldown = false;

    }//waitco

    IEnumerator FootStepWait(){

        yield return new WaitForSeconds(stepspeed);

        if(movevector != Vector3.zero){
            //Object step = steplib[Random.Range(0,steplib.Length)] as AudioClip;
            //AudioFX.FXOneShot("Footsteeps/" + floortype + "/" + step.name , this.gameObject);
        }

        stepshot = false;
        
    }


}//eof
