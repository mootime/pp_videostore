﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Task : MonoBehaviour
{

    public string taskname;
    public bool[] steps;
    public string[] stepname;

    public bool taskdone = false;

    public bool multi_collect = false;
    public int collect_count = 0;

    public bool dev_taskdone = false;

    public string current_subtask = "";

    void Start (){

        EventSystem.Event.onSubTaskCompleted += SubTaskCompleted;

        taskname = gameObject.name;

        if(stepname.Length != 1){
            current_subtask = stepname[0];
            Debug.Log(stepname.Length);
        }

            if(multi_collect == true){
                current_subtask = ( collect_count + "/" + steps.Length.ToString() );
            }

    }

    void FixedUpdate() {
        
        if (dev_taskdone == true && taskdone == false)
        {
            taskdone = true;
            EventSystem.Event.TaskCompleted(this.gameObject);   
        }

    }

    public void IsTaskComplete() {

        for (int i = 0; i < steps.Length; i++)
        {

            if(steps[i] == true){

                //check next

            }
            else{

                current_subtask = stepname[i];

                if(multi_collect == true){
                    collect_count ++;
                    current_subtask = ( collect_count + "/" + steps.Length.ToString() );
                }

                return;

            }

        }//loop

            //prevent sending the message more then once
            if(taskdone == false){

                EventSystem.Event.TaskCompleted(this.gameObject);
                current_subtask = "COMPLETE";
                Debug.Log(taskname + " Complete");
                
            }

            taskdone = true;
    }

    public void SubTaskCompleted (string parenttask, int stepnumber){

        if(parenttask == taskname){

            steps[stepnumber] = true;
            IsTaskComplete();

        }

    }

}//eof
