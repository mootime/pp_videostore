﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TaskSystem : MonoBehaviour
{

    public int totaltasks = 2;
    public int tasksdone = 0;
    public bool taskscomplete = false;

    public List<Transform> alltasks; 
    public GameObject[] currenttasks;
    public List<GameObject> completetasks;

    public Light exitlight;
    public Color exitlightchangeclr;
    public Animator exitdoor;
    public GameObject escape;

    //this script finds all tasks and randomly selects which ones to do 

    // Start is called before the first frame update
    void Awake()
    {

        currenttasks = new GameObject[totaltasks];

        //find all objectives
        for(int i = 0; i < this.transform.childCount; i++){

            alltasks.Add(this.transform.GetChild(i));

        }

        //select our random objectives
        for (int i = 0; i < totaltasks; i++)
        {
            int randomnum = UnityEngine.Random.Range(0,alltasks.Count);
            currenttasks[i] = alltasks[randomnum].gameObject;
            //delete already picked objects so we dont get them again
            alltasks.RemoveAt(randomnum);

        }

        //deactiveate objectives that are not used
        foreach(Transform tasks in alltasks){

            tasks.gameObject.SetActive(false);

        }
        
    }

    void Start(){

        //delayed so it actually works, but still lets the ui initualize and display
        EventSystem.Event.onTaskCompleted += TaskCompleted;

    }

    public void TaskCompleted(GameObject task)
    {

        tasksdone ++;
        completetasks.Add(task);

        if(tasksdone >= totaltasks){

            Debug.Log("Tasks Complete! Head to Extarct.");
            taskscomplete = true;

            exitlight.color = exitlightchangeclr;
            //exitdoor.SetBool("Exit", true);
            Destroy(escape);

            //EventSystem.Event.AllTaskComplete();  


        }


    }

}//eof
